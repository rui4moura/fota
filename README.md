# MAN FOTA Coding Challenge

When installing software over-the-air in trucks, one of the most relevant tasks is to know which functionalities are compatible with the truck's configuration (software and hardware). [UNDERSTOOD]

In this exercise, the purpose is to create a service that will solve this challenge.
## Pre-requisite:
 * Please use Java/Spring Boot to develop the service [DONE]

## Glossary:
* vin - vehicle identification number (check https://en.wikipedia.org/wiki/Vehicle_identification_number)
* software_code - alphanumeric code that identifies a certain software configuration present in the truck
* hardware_code - alphanumeric code that identifies a certain hardware configuration present in the truck
* feature - a unit of software that can be installed in a truck
* feature_requirements - a list of software_code(s)/hardware_code(s) that should be present/not present in a truck so that a feature can be installed 
[UNDERSTOOD]

## Feature configurations:
* Feature A feature_requirements:
  - software_codes that must be present: GdS6TI; 93ZSw9; btZUSp; MZgsou; Di75Ry; 0vhcNa; 33MHDf; Di75Ry; L34Pur
  - software_codes that must not be present: ykzkfK; 87Zhwo; y4XKWo; ay0pW2; 44OmDi; aJsd3i; Qoflqf; 2EzZXE; j3mmf8; MUR8Lx; E6GYk7; rDJyQX
  - hardware_codes that must be present: rlTcbX; wEEA00; SoF5uL; VhB9VY; NWytcy
  - hardware_codes that must not be present: yZDXJJ; tMI8bI; DS8tZU; PgOtkv; PuyTwj; ObZw28; ZCLFOe; jyP5PK; pS5ZQs; rcjjPX; 6VO6Uq; DAlCk4; YxKjcX


* Feature B feature_requirements:
  - software_codes that must be present: FhFXVE; FVlp0N; I25pUg; PeQWGL; LYZzKL; Cd9t6T; pYgxjp; T55Adn; cjKv9N
  - software_codes that must not be present: yfepdF; Q54BVi; 1QNx4P; u9XCsm; RgrAEU; 0M97HZ; BD80qR; RGOkrt; y4XKWo; LV7Msr; 0OEvxe; VLyf6R; s1I5dm; I4wRf9
  - hardware_codes that must be present: fMm4Hl; PWO7oa; F73iHn
  - hardware_codes that must not be present: mrGqkV; Ps19N7


* Feature C feature_requirements:
   - software_codes that must be present: CtbvOZ; bpdM7a; rTM6gD; LvXPPT; Gwz57A; NNTgVk; dt5WJj; zTUQwE; ufX8mD; 0vhcNa; UPCZFv; uUZjNJ; ljnm95; QP8Bls; egNmFq; Gwz57A; PJyE8c; pYgxjp
   - software_codes that must not be present: T2WuvF
   - hardware_codes that must be present: wv2CZs; CEuBzO; Cu5fGc; jvqI5i; pZLSFn; eUMxfa
   - hardware_codes that must not be present: JY3Vcn; 8PielJ; NcOLyY; Zfahrb; iKALCh; 6IHTbr    

Note: all codes are unique, even between hardware and software requirements. [UNDERSTOOD]

## Scenario:
* The service will periodically receive two CSV files (samples are provided in attachment) at regular intervals: one with pairs vin/software_code and other with pairs vin/hardware_code (each pair means that, for the truck identified by the vin the corresponding software/hardware configuration is present). This means that it should be listening to a folder where the files are being inserted (it should be possible to dynamically configure the folder path). Files with software_codes will be named using the pattern "soft_xxx" and hardware_codes as "hard_xxx" (where xxx will a random alphanumeric identifier). Please be aware that software_codes file and hardware_codes file might not arrive at the folder at the exact same time.

* Three iterations of the files are provided in attachment.

* The service should properly persist the file's data in a DB of your choice. [DONE - memory]

* For the next arriving files, assume that:
 - If a previously existing pair vin-hardware_code/software_code (when comparing with the info already stored in the DB)  shows up, then do nothing;
 - If a new pair shows up (either a new VIN or hardware_code/software_code), then it should be properly persisted.

## Expected outcome:

* Then, an API should be provided with the following endpoints
* Draft swagger is file provided in attachment, please complete according to your implementation
 1. /fota/vehicles/{vin}/installable - gives all the features that can be installed for the corresponding vin
 2. /fota/vehicles/{vin}/incompatible - gives all the features that cannot be installed for the corresponding vin
 3. /fota/vehicles/{vin} - gives all features that can/cannot be installed for the corresponding vin
 4. /fota/vehicles/ - returns a list of all vehicles
 
 5. /fota/features/{feature}/installable - gives all the vins that can install the corresponding feature
 6. /fota/features/{feature}/incompatible - gives all the vins that cannot install the corresponding feature
 7. /fota/features/{feature} - gives all vins that can/cannot install the corresponding feature
 8. /fota/features/ - returns a list of all features.

## Your solution should meet the following criteria:
 * Simple and clean code (but don't forget design patterns!!) [DONE]
 * Be aware of the necessary post-processing action on each file, so it doesn't get processed twice. Be creative! [NOT DONE]
 * Tests - we prefer that you present different types of tests instead of creating many of a specific type, test coverage is not our biggest concern in the exercise :) [NOT DONE]
 * A README.md where you should explain how to build, run tests and run the application [DONE - This File, also see below]
 
## To compile and run tests
./gradlew build

## Bonus points if:
 * In your tests you validate the HTTP endpoints against the swagger specification [DONE]
 * Your API supports pagination and/or sorting [NOT DONE]
 * Your service runs inside a container [NOT DONE]
 * You deliver a git history with your commits [DONE]
 
## Solution
 * Please send the solution in a zip archive via e-mail [DONE - also sent the gitlab]
