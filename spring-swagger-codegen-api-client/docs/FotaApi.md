# FotaApi

All URIs are relative to *https://fota.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllFeatures**](FotaApi.md#getAllFeatures) | **GET** /fota/features | Find all features
[**getAllVin**](FotaApi.md#getAllVin) | **GET** /fota/features/{feature} | Find all VIN&#39;s by feature code
[**getFeatures**](FotaApi.md#getFeatures) | **GET** /fota/vehicles/{vin} | Find all features by VIN
[**getIncompatible**](FotaApi.md#getIncompatible) | **GET** /fota/vehicles/{vin}/incompatible | Find incompatible features by VIN
[**getInstallable**](FotaApi.md#getInstallable) | **GET** /fota/vehicles/{vin}/installable | Find installable features by VIN
[**getVehicles**](FotaApi.md#getVehicles) | **GET** /fota/vehicles | Find all vehicles
[**getVinIncompatible**](FotaApi.md#getVinIncompatible) | **GET** /fota/features/{feature}/incompatible | Find incompatible VIN&#39;s by feature code
[**getVinInstallable**](FotaApi.md#getVinInstallable) | **GET** /fota/features/{feature}/installable | Find installable VIN&#39;s by feature code


<a name="getAllFeatures"></a>
# **getAllFeatures**
> getAllFeatures(feature)

Find all features

returns a list of all feature codes

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
String feature = "feature_example"; // String | Correspondent feature code
try {
    apiInstance.getAllFeatures(feature);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getAllFeatures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feature** | **String**| Correspondent feature code |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllVin"></a>
# **getAllVin**
> getAllVin(feature)

Find all VIN&#39;s by feature code

gives all the vins that can install the corresponding feature

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
String feature = "feature_example"; // String | Correspondent feature code
try {
    apiInstance.getAllVin(feature);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getAllVin");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feature** | **String**| Correspondent feature code |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFeatures"></a>
# **getFeatures**
> getFeatures(vin)

Find all features by VIN

gives all features that can/cannot be installed for the corresponding vin

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
UUID vin = new UUID(); // UUID | Vehicle identification number
try {
    apiInstance.getFeatures(vin);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getFeatures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vin** | **UUID**| Vehicle identification number |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getIncompatible"></a>
# **getIncompatible**
> getIncompatible(vin)

Find incompatible features by VIN

gives all the features that cannot be installed for the corresponding vin

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
UUID vin = new UUID(); // UUID | Vehicle identification number
try {
    apiInstance.getIncompatible(vin);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getIncompatible");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vin** | **UUID**| Vehicle identification number |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getInstallable"></a>
# **getInstallable**
> getInstallable(vin)

Find installable features by VIN

gives all the features that can be installed for the corresponding vin

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
UUID vin = new UUID(); // UUID | Vehicle identification number
try {
    apiInstance.getInstallable(vin);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getInstallable");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vin** | **UUID**| Vehicle identification number |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVehicles"></a>
# **getVehicles**
> getVehicles()

Find all vehicles

returns a list of all vehicles

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
try {
    apiInstance.getVehicles();
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getVehicles");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVinIncompatible"></a>
# **getVinIncompatible**
> getVinIncompatible(feature)

Find incompatible VIN&#39;s by feature code

gives all the vins that cannot install the corresponding feature

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
String feature = "feature_example"; // String | Correspondent feature code
try {
    apiInstance.getVinIncompatible(feature);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getVinIncompatible");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feature** | **String**| Correspondent feature code |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVinInstallable"></a>
# **getVinInstallable**
> getVinInstallable(feature)

Find installable VIN&#39;s by feature code

gives all the vins that can install the corresponding feature

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FotaApi;


FotaApi apiInstance = new FotaApi();
String feature = "feature_example"; // String | Correspondent feature code
try {
    apiInstance.getVinInstallable(feature);
} catch (ApiException e) {
    System.err.println("Exception when calling FotaApi#getVinInstallable");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feature** | **String**| Correspondent feature code |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

