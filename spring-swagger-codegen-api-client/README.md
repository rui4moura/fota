# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.FotaApi;

import java.io.File;
import java.util.*;

public class FotaApiExample {

    public static void main(String[] args) {
        
        FotaApi apiInstance = new FotaApi();
        String feature = "feature_example"; // String | Correspondent feature code
        try {
            apiInstance.getAllFeatures(feature);
        } catch (ApiException e) {
            System.err.println("Exception when calling FotaApi#getAllFeatures");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://fota.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*FotaApi* | [**getAllFeatures**](docs/FotaApi.md#getAllFeatures) | **GET** /fota/features | Find all features
*FotaApi* | [**getAllVin**](docs/FotaApi.md#getAllVin) | **GET** /fota/features/{feature} | Find all VIN&#39;s by feature code
*FotaApi* | [**getFeatures**](docs/FotaApi.md#getFeatures) | **GET** /fota/vehicles/{vin} | Find all features by VIN
*FotaApi* | [**getIncompatible**](docs/FotaApi.md#getIncompatible) | **GET** /fota/vehicles/{vin}/incompatible | Find incompatible features by VIN
*FotaApi* | [**getInstallable**](docs/FotaApi.md#getInstallable) | **GET** /fota/vehicles/{vin}/installable | Find installable features by VIN
*FotaApi* | [**getVehicles**](docs/FotaApi.md#getVehicles) | **GET** /fota/vehicles | Find all vehicles
*FotaApi* | [**getVinIncompatible**](docs/FotaApi.md#getVinIncompatible) | **GET** /fota/features/{feature}/incompatible | Find incompatible VIN&#39;s by feature code
*FotaApi* | [**getVinInstallable**](docs/FotaApi.md#getVinInstallable) | **GET** /fota/features/{feature}/installable | Find installable VIN&#39;s by feature code


## Documentation for Models



## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



