package domain;

import domain.jpa.PartCode;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PartCodeRepository extends CrudRepository<PartCode, Long> {

    List<PartCode> partCodes();

    PartCode findById(long id);
}
