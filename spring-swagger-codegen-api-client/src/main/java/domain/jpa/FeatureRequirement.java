package domain.jpa;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.UUID;

@Entity
public class FeatureRequirement {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private UUID id;
    private String name;
    private Collection<PartCode> includedSoftwareCodes;
    private Collection<PartCode> excludedSoftwareCodes;
    private Collection<PartCode> includedHardwareCodes;
    private Collection<PartCode> excludedHardwareCodes;

    protected FeatureRequirement() {

    }

    public FeatureRequirement(UUID id, String name, Collection<PartCode> includedSoftwareCodes,
            Collection<PartCode> excludedSoftwareCodes, Collection<PartCode> includedHardwareCodes,
            Collection<PartCode> excludedHardwareCodes) {
        this.id = id;
        this.name = name;
        this.includedSoftwareCodes = includedSoftwareCodes;
        this.excludedSoftwareCodes = excludedSoftwareCodes;
        this.includedHardwareCodes = includedHardwareCodes;
        this.excludedHardwareCodes = excludedHardwareCodes;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, vin='%s']",
                id, name);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "", fetch = FetchType.LAZY)
    public Collection<PartCode> getIncludedSoftwareCodes() {
        return includedSoftwareCodes;
    }

    public void setIncludedSoftwareCodes(Collection<PartCode> includedSoftwareCodes) {
        this.includedSoftwareCodes = includedSoftwareCodes;
    }

    @OneToMany(mappedBy = "partCode", fetch = FetchType.LAZY)
    public Collection<PartCode> getExcludedSoftwareCodes() {
        return excludedSoftwareCodes;
    }

    public void setExcludedSoftwareCodes(Collection<PartCode> excludedSoftwareCodes) {
        this.excludedSoftwareCodes = excludedSoftwareCodes;
    }

    @OneToMany(mappedBy = "partCode", fetch = FetchType.LAZY)
    public Collection<PartCode> getIncludedHardwareCodes() {
        return includedHardwareCodes;
    }

    public void setIncludedHardwareCodes(Collection<PartCode> includedHardwareCodes) {
        this.includedHardwareCodes = includedHardwareCodes;
    }

    @OneToMany(mappedBy = "partCode", fetch = FetchType.LAZY)
    public Collection<PartCode> getExcludedHardwareCodes() {
        return excludedHardwareCodes;
    }

    public void setExcludedHardwareCodes(Collection<PartCode> excludedHardwareCodes) {
        this.excludedHardwareCodes = excludedHardwareCodes;
    }
}
