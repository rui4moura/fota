package domain.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Vin {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private UUID id;
    private String vin;

    protected Vin() {

    }

    public Vin(UUID id, String vin) {
        this.id = id;
        this.vin = vin;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, vin='%s']",
                id, vin);
    }

    public UUID getId() {
        return id;
    }

    public String getVin() {
        return vin;
    }

}
