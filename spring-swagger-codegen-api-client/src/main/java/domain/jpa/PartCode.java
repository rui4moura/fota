package domain.jpa;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
public class PartCode {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private UUID id;
    private String code;
    private Enum codeType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private FeatureRequirement includedSoftwareCode;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private FeatureRequirement excludedSoftwareCode;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private FeatureRequirement includedHardwareCode;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private FeatureRequirement excludedHardwareCode;

    protected PartCode() {

    }

    public PartCode(UUID id, String code, Enum codeType) {
        this.id = id;
        this.code = code;
        this.codeType = codeType;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, code='%s', codeType='%s']",
                id, code, codeType);
    }

    public UUID getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Enum getCodeType() {
        return codeType;
    }
}
